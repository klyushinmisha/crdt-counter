package main

import (
	"math/rand"
	"time"

	"go.uber.org/zap"
)

const (
	Replicas   = 4
	Iterations = 5
	SmallLag   = 20 * time.Millisecond
	BigLag     = 100 * time.Millisecond
)

var (
	incReq        = make([]chan int, 0)
	incReplicaReq = make([]chan int, 0)
	getReq        = make([]chan struct{}, 0)
	getRsp        = make([]chan int, 0)
	logger, _     = zap.NewProduction()
	log           = logger.Sugar()
)

func jitter(t time.Duration) time.Duration {
	return time.Duration(float32(t) * rand.Float32())
}

func runner(i int) {
	local := 0

	for {
		select {
		case v := <-incReq[i]:
			local += v
			log.Infow(
				"Increment",
				"runner", i,
				"crdt-value", local,
			)
			for j := 0; j < Replicas; j++ {
				if i == j {
					continue
				}
				time.Sleep(jitter(BigLag))
				incReplicaReq[j] <- v
			}
		case v := <-incReplicaReq[i]:
			local += v
			log.Infow(
				"Increment replica",
				"runner", i,
				"crdt-value", local,
			)
		case <-getReq[i]:
			getRsp[i] <- local
		default:
			time.Sleep(jitter(SmallLag))
		}
	}
}

func main() {
	defer logger.Sync()

	for i := 0; i < Replicas; i++ {
		incReq = append(incReq, make(chan int, 10))
		incReplicaReq = append(incReplicaReq, make(chan int, 10))
		getReq = append(getReq, make(chan struct{}, 10))
		getRsp = append(getRsp, make(chan int, 10))
	}

	for i := 0; i < Replicas; i++ {
		go runner(i)
	}

	for n := 0; n < Iterations; n++ {
		incReq[rand.Int()%Replicas] <- (rand.Int() % 10) + 1
		time.Sleep(SmallLag)
		for i := 0; i < Replicas; i++ {
			getReq[i] <- struct{}{}
			log.Infow(
				"Get",
				"runner", i,
				"crdt-value", <-getRsp[i],
			)
		}
	}

	time.Sleep(time.Second)

	for i := 0; i < Replicas; i++ {
		getReq[i] <- struct{}{}
		log.Infow(
			"Get",
			"runner", i,
			"crdt-value", <-getRsp[i],
		)
	}
}

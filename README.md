# crdt-counter

CRDT counter pet-project

## Example

```bash
$ go run main.go
{"level":"info","ts":1648317961.2483659,"caller":"crdt-counter/main.go:37","msg":"Increment","runner":2,"crdt-value":2}
{"level":"info","ts":1648317961.2783077,"caller":"crdt-counter/main.go:83","msg":"Get","runner":0,"crdt-value":0}
{"level":"info","ts":1648317961.2793949,"caller":"crdt-counter/main.go:83","msg":"Get","runner":1,"crdt-value":0}
{"level":"info","ts":1648317961.3336277,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":0,"crdt-value":2}
{"level":"info","ts":1648317961.3736985,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":1,"crdt-value":2}
{"level":"info","ts":1648317961.4004989,"caller":"crdt-counter/main.go:83","msg":"Get","runner":2,"crdt-value":2}
{"level":"info","ts":1648317961.4100218,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":3,"crdt-value":2}
{"level":"info","ts":1648317961.410054,"caller":"crdt-counter/main.go:83","msg":"Get","runner":3,"crdt-value":2}
{"level":"info","ts":1648317961.4279463,"caller":"crdt-counter/main.go:37","msg":"Increment","runner":0,"crdt-value":3}
{"level":"info","ts":1648317961.525594,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":1,"crdt-value":3}
{"level":"info","ts":1648317961.5457017,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":2,"crdt-value":3}
{"level":"info","ts":1648317961.6081545,"caller":"crdt-counter/main.go:83","msg":"Get","runner":0,"crdt-value":3}
{"level":"info","ts":1648317961.6133087,"caller":"crdt-counter/main.go:83","msg":"Get","runner":1,"crdt-value":3}
{"level":"info","ts":1648317961.6144087,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":3,"crdt-value":3}
{"level":"info","ts":1648317961.61758,"caller":"crdt-counter/main.go:83","msg":"Get","runner":2,"crdt-value":3}
{"level":"info","ts":1648317961.6198556,"caller":"crdt-counter/main.go:83","msg":"Get","runner":3,"crdt-value":3}
{"level":"info","ts":1648317961.631213,"caller":"crdt-counter/main.go:37","msg":"Increment","runner":2,"crdt-value":10}
{"level":"info","ts":1648317961.637625,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":0,"crdt-value":10}
{"level":"info","ts":1648317961.6532836,"caller":"crdt-counter/main.go:83","msg":"Get","runner":0,"crdt-value":10}
{"level":"info","ts":1648317961.6596255,"caller":"crdt-counter/main.go:83","msg":"Get","runner":1,"crdt-value":3}
{"level":"info","ts":1648317961.7425854,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":1,"crdt-value":10}
{"level":"info","ts":1648317961.7426207,"caller":"crdt-counter/main.go:83","msg":"Get","runner":2,"crdt-value":10}
{"level":"info","ts":1648317961.7436695,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":3,"crdt-value":10}
{"level":"info","ts":1648317961.743683,"caller":"crdt-counter/main.go:83","msg":"Get","runner":3,"crdt-value":3}
{"level":"info","ts":1648317961.746967,"caller":"crdt-counter/main.go:37","msg":"Increment","runner":3,"crdt-value":18}
{"level":"info","ts":1648317961.7637084,"caller":"crdt-counter/main.go:83","msg":"Get","runner":0,"crdt-value":10}
{"level":"info","ts":1648317961.7688687,"caller":"crdt-counter/main.go:83","msg":"Get","runner":1,"crdt-value":10}
{"level":"info","ts":1648317961.769996,"caller":"crdt-counter/main.go:83","msg":"Get","runner":2,"crdt-value":10}
{"level":"info","ts":1648317961.8012998,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":0,"crdt-value":18}
{"level":"info","ts":1648317961.8423562,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":1,"crdt-value":18}
{"level":"info","ts":1648317961.9253151,"caller":"crdt-counter/main.go:83","msg":"Get","runner":3,"crdt-value":18}
{"level":"info","ts":1648317961.9325206,"caller":"crdt-counter/main.go:37","msg":"Increment","runner":0,"crdt-value":24}
{"level":"info","ts":1648317961.9336207,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":2,"crdt-value":18}
{"level":"info","ts":1648317962.0066872,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":1,"crdt-value":24}
{"level":"info","ts":1648317962.0924618,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":2,"crdt-value":24}
{"level":"info","ts":1648317962.0946724,"caller":"crdt-counter/main.go:83","msg":"Get","runner":0,"crdt-value":24}
{"level":"info","ts":1648317962.1007855,"caller":"crdt-counter/main.go:83","msg":"Get","runner":1,"crdt-value":24}
{"level":"info","ts":1648317962.1029482,"caller":"crdt-counter/main.go:51","msg":"Increment replica","runner":3,"crdt-value":24}
{"level":"info","ts":1648317962.1062274,"caller":"crdt-counter/main.go:83","msg":"Get","runner":2,"crdt-value":24}
{"level":"info","ts":1648317962.1073554,"caller":"crdt-counter/main.go:83","msg":"Get","runner":3,"crdt-value":24}
{"level":"info","ts":1648317963.123589,"caller":"crdt-counter/main.go:95","msg":"Get","runner":0,"crdt-value":24}
{"level":"info","ts":1648317963.1321292,"caller":"crdt-counter/main.go:95","msg":"Get","runner":1,"crdt-value":24}
{"level":"info","ts":1648317963.1394851,"caller":"crdt-counter/main.go:95","msg":"Get","runner":2,"crdt-value":24}
{"level":"info","ts":1648317963.140554,"caller":"crdt-counter/main.go:95","msg":"Get","runner":3,"crdt-value":24}
```
